# Nom du projet
Récupération de trajets GPS 

## Objectif
Les objectifs à atteindre durant le TP sont : 
-	Décoder un fichiers binaires
-	Récupérer une liste de points GPS 
-	Ajouter ces valeurs dans un fichier CSV

# Comment démarrer le projet ? 
Pour démarrer le projet, il faut seulement éxécuter le programme scipt

# Le résultat
N'ayant pas terminé, le résultat est l'obtention d'un fichier CSV correspondant aux points GPS du fichier binaire T0.

# Explication du code
Le coeur du programme réside dans la fonction trace_gps(). Pour pouvoir utiliser correctement la fonction, voici l'explication des différents paramètres : 

1. nom_fichier -> nom du fichier binaire dont on veut récupérer la trace GPS
2. nom_fichier_csv -> nom du fichier où l'on souhaite stocker le trajet GPS
3. adresse_debut_hex -> adresse en hexadécimale du premier point GPS
4. adresse_fin_hex -> adresse en héxadécimale du dernier point GPS
5. intervalle_hex -> intervalle en hexadécimale entre points GPS 

