#include <stdio.h>
#include <stdlib.h>

void trace_gps(char * nom_fichier, char * nom_fichier_csv,char * adresse_debut_hex, char * adresse_fin_hex, char * intervalle_hex)
{
    FILE *fichier;
    FILE * fichier_csv;

    long offset; 
    long intervalle; 
    long fin;
    int num = 1;
    
    offset = strtol(adresse_debut_hex, NULL, 16);
    intervalle = strtol(intervalle_hex, NULL, 16);
    fin = strtol(adresse_fin_hex, NULL, 16);
    
    fichier = fopen(nom_fichier, "rb");
    fichier_csv = fopen(nom_fichier_csv,"w+");
    if (fichier == NULL || fichier_csv == NULL) 
    {
        printf("Impossible d'ouvrir le fichier.\n");
    }
    fprintf(fichier_csv,"numero;latitude;longitude\n");
    
    while (1) 
    {
        fseek(fichier, offset, SEEK_SET);
        
        int latitude, longitude;
        fread(&latitude,sizeof(int), 1, fichier);
        fread(&longitude,sizeof(int), 1, fichier);
        fprintf(fichier_csv,"%d;%d;%d\n",num,latitude,longitude);
        //printf("Valeur GPS numero %d : (%d,%d)\n",num,latitude, longitude);
        
        offset += intervalle;
        if(offset>fin)
        {
            break;
        }
        num++;
    }
    fclose(fichier);
}

int main()
{
    trace_gps("./fichiers_binaires/13caaf25c97e29c8b45ae2e4f9f8ac3b_T0_2e526","./fichiers_CSV/T0.csv","0x667" ,"0x592E" , "0x31");
    return 0;
}